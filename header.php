<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Krohn-Leitmannstetter_Karriere_Theme
 */
 $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
    <!--<script src='https://www.google.com/recaptcha/api.js'></script>-->
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'klcareers' ); ?></a>

    <!-- HEADER -->
    <header class="site-header" role="banner">

        <!-- NAVBAR -->
        <div class="navbar-wrapper">
            <div class="navbar navbar-fixed-top" role="navigation" style="background-color:white;">
                <div class="container-fluid">
                    <div class="navbar-header" style="float: right;">
                        <div id="google_translate_element" style="display: <?php echo($language != 'de' ? 'block' : 'none')?>;">
                        </div>
                        <script type="text/javascript">
                            function googleTranslateElementInit() {
                                new google.translate.TranslateElement({
                                    pageLanguage: 'de',
                                    layout: google.translate.TranslateElement.InlineLayout.SIMPLE
                                }, 'google_translate_element');
                            }
                        </script>

                        <script type="text/javascript"
                                src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        <!--
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>-->

                    </div><!-- navbar-header -->

					<?php
					wp_nav_menu( array(

						'menu_location'   => 'primary',
						'container'       => 'nav',
						'container_class' => 'navbar-collapse collapse',
						'menu_class'      => 'nav navbar-nav'

					) );
					?>

                </div><!-- container-->
            </div><!-- navbar -->
        </div><!-- navbar-wrapper -->

    </header>
