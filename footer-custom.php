<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bootstrap_to_Wordpress
 */

// Advanced Custom Fields
// FOOTER
global $wp_query;
$post_id                    = $wp_query->post->ID;
$footer_social_ueberschrift = get_post_meta( $post_id, 'footer_social_ueberschrift', true );
$footer_social_title        = get_post_meta( $post_id, 'footer_social_title', true );
$footer_social_text         = get_post_meta( $post_id, 'footer_social_text', true );
$footer_social_link         = get_post_meta( $post_id, 'footer_social_link', true );
$footer_social_bild         = get_post_meta( $post_id, 'footer_social_bild', true );
$footer_copyright           = get_post_meta( $post_id, 'footer_copyright', true );
$footer_impressum_link      = get_post_meta( $post_id, 'footer_impressum_link', true );

?>

<?php wp_footer(); ?>

<!-- FOOTER -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="footer-title"><?php echo $footer_social_ueberschrift ?></h3>
            </div>
        </div>
        <ul class="row footer-social-list" role="tablist">
            <li class="col-md-2 col-sm-12 footer-social-link">
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink( $footer_social_link ) ?>"
                   target="_blank">FACEBOOK</a>
            </li>
            <li class="col-md-2 col-sm-12 footer-social-link">
                <a href="https://twitter.com/intent/tweet?text=<?php echo $footer_social_text ?>&url=<?php echo get_the_permalink( $footer_social_link ) ?>"
                   target="_blank">TWITTER</a>
            </li>
            <li class="col-md-2 col-sm-12 footer-social-link">
                <a href="https://www.xing.com/spi/shares/new?url=<?php echo get_the_permalink( $footer_social_link ) ?>"
                   target="_blank">XING</a>
            </li>
            <li class="col-md-2 col-sm-12 footer-social-link">
                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_the_permalink( $footer_social_link ) ?>&title=<?php echo $footer_social_title ?>&summary=<?php echo $footer_social_text ?>"
                   target="_blank">LINKEDIN</a>
            </li>
            <li class="col-md-2 col-sm-12 footer-social-link">
                <a href="http://pinterest.com/pin/create/button/?url=<?php echo get_the_permalink( $footer_social_link ) ?>&media=<?php echo wp_get_attachment_image_src( $footer_social_bild, 'medium_large' )[0] ?>&description=<?php echo $footer_social_text ?>"
                   target="_blank">PINTEREST</a>
            </li>
            <li class="col-md-2 col-sm-12 footer-social-link">
                <a href="https://plus.google.com/share?url=<?php echo get_the_permalink( $footer_social_link ) ?>"
                   target="_blank">GOOGLE</a>
            </li>
        </ul>
        <div class="row footer-imprint-row">
            <div class="col-sm-12">
                <p><?php echo $footer_copyright ?> <a href="<?php echo $footer_impressum_link ?>"
                                                      class="footer-imprint-link" target="_blank">Impressum</a></p>
            </div>
        </div>
    </div>
</footer>

<!-- BOOTSTRAP CORE JAVASCRIPT -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/jquery-3.2.1.min.js"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/scrollreveal.min.js"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/jquery.waypoints.min.js"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/jquery.counterup.js"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/main.js"></script>
<script src="<?php bloginfo( 'template_directory' ); ?>/assets/js/bootstrap-select.min.js"></script>
<!--<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAN3i6jLDfdGWjsPqLjPs3DnBzLwxOYFMY&callback=initMap"></script>-->
<script type="text/javascript">
    $("#myModal").on('hidden.bs.modal', function (e) {
        $("#myModal iframe").attr("src", $("#myModal iframe").attr("src"));
    });
</script>
<!--<script>

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-111528790-1', 'jobs.krohn-leitmannstetter.de');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');

</script>-->
</body>
</html>
