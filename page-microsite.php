<?php
/**
 * Template Name: Karriere Microsite
 */
// Advanced Custom Fields
// AUFMACHER
$aufmacher_bild = get_field( 'aufmacher_bild' );

// SPOT
$spot_bild         = get_field( 'spot_bild' );
$spot_youtube_link = get_field( 'spot_youtube_link' );

// STELLEN
$stellen_ueberschrift   = get_field( 'stellen_ueberschrift' );
$stellen_untertitel     = get_field( 'stellen_untertitel' );
$stellen_kategorie_1    = get_field( 'stellen_kategorie_1' );
$stellen_in_kategorie_1 = get_field( 'stellen_in_kategorie_1' );
$stellen_kategorie_2    = get_field( 'stellen_kategorie_2' );
$stellen_in_kategorie_2 = get_field( 'stellen_in_kategorie_2' );
$stellen_kategorie_3    = get_field( 'stellen_kategorie_3' );
$stellen_in_kategorie_3 = get_field( 'stellen_in_kategorie_3' );

// EINRICHTUNGEN
$einrichtungen_titel            = get_field( 'einrichtungen_titel' );
$einrichtungen_text             = get_field( 'einrichtungen_text' );
$einrichtungen_slideshow_bild_1 = get_field( 'einrichtungen_slideshow_bild_1' );
$einrichtungen_slideshow_bild_2 = get_field( 'einrichtungen_slideshow_bild_2' );
$einrichtungen_slideshow_bild_3 = get_field( 'einrichtungen_slideshow_bild_3' );

// STELLENANZEIGEN
$stellenanzeigen_title           = get_field( 'stellenanzeigen_title' );
$stellenanzeigen_bewerben_button = get_field( 'stellenanzeigen_bewerben_button' );

// Neuigkeiten
$neuigkeiten_title  = get_field( 'neuigkeiten_title' );
$neuigkeiten_text   = get_field( 'neuigkeiten_text' );
$neuigkeiten_button = get_field( 'neuigkeiten_button' );

// FAQS
$faqs_ueberschrift  = get_field( 'faqs_ueberschrift' );
$faqs_faq1_frage    = get_field( 'faqs_faq1_frage' );
$faqs_faq1_antwort  = get_field( 'faqs_faq1_antwort' );
$faqs_faq2_frage    = get_field( 'faqs_faq2_frage' );
$faqs_faq2_antwort  = get_field( 'faqs_faq2_antwort' );
$faqs_faq3_frage    = get_field( 'faqs_faq3_frage' );
$faqs_faq3_antwort  = get_field( 'faqs_faq3_antwort' );
$faqs_faq4_frage    = get_field( 'faqs_faq4_frage' );
$faqs_faq4_antwort  = get_field( 'faqs_faq4_antwort' );
$faqs_faq5_frage    = get_field( 'faqs_faq5_frage' );
$faqs_faq5_antwort  = get_field( 'faqs_faq5_antwort' );
$faqs_faq6_frage    = get_field( 'faqs_faq6_frage' );
$faqs_faq6_antwort  = get_field( 'faqs_faq6_antwort' );
$faqs_faq7_frage    = get_field( 'faqs_faq7_frage' );
$faqs_faq7_antwort  = get_field( 'faqs_faq7_antwort' );
$faqs_faq8_frage    = get_field( 'faqs_faq8_frage' );
$faqs_faq8_antwort  = get_field( 'faqs_faq8_antwort' );
$faqs_faq9_frage    = get_field( 'faqs_faq9_frage' );
$faqs_faq9_antwort  = get_field( 'faqs_faq9_antwort' );
$faqs_faq10_frage   = get_field( 'faqs_faq10_frage' );
$faqs_faq10_antwort = get_field( 'faqs_faq10_antwort' );

// FAKTEN
$fakten_ueberschrift = get_field( 'fakten_ueberschrift' );
$fakten_untertitel   = get_field( 'fakten_untertitel' );
$fakten_fakt1_icon   = get_field( 'fakten_fakt1_icon' );
$fakten_fakt1_zahl   = get_field( 'fakten_fakt1_zahl' );
$fakten_fakt1_text   = get_field( 'fakten_fakt1_text' );
$fakten_fakt2_icon   = get_field( 'fakten_fakt2_icon' );
$fakten_fakt2_zahl   = get_field( 'fakten_fakt2_zahl' );
$fakten_fakt2_text   = get_field( 'fakten_fakt2_text' );
$fakten_fakt3_icon   = get_field( 'fakten_fakt3_icon' );
$fakten_fakt3_zahl   = get_field( 'fakten_fakt3_zahl' );
$fakten_fakt3_text   = get_field( 'fakten_fakt3_text' );
$fakten_fakt4_icon   = get_field( 'fakten_fakt4_icon' );
$fakten_fakt4_zahl   = get_field( 'fakten_fakt4_zahl' );
$fakten_fakt4_text   = get_field( 'fakten_fakt4_text' );

// UNTERNEHMEN
$unternehmen_titel                   = get_field( 'unternehmen_titel' );
$unternehmen_text                    = get_field( 'unternehmen_text' );
$unternehmen_bild                    = get_field( 'unternehmen_bild' );
$unternehmen_leitbild_ueberschrift   = get_field( 'unternehmen_leitbild_ueberschrift' );
$unternehmen_leitbild_1_ueberschrift = get_field( 'unternehmen_leitbild_1_ueberschrift' );
$unternehmen_leitbild_1_text         = get_field( 'unternehmen_leitbild_1_text' );
$unternehmen_leitbild_1_icon         = get_field( 'unternehmen_leitbild_1_icon' );
$unternehmen_leitbild_2_ueberschrift = get_field( 'unternehmen_leitbild_2_ueberschrift' );
$unternehmen_leitbild_2_text         = get_field( 'unternehmen_leitbild_2_text' );
$unternehmen_leitbild_2_icon         = get_field( 'unternehmen_leitbild_2_icon' );
$unternehmen_leitbild_3_ueberschrift = get_field( 'unternehmen_leitbild_3_ueberschrift' );
$unternehmen_leitbild_3_text         = get_field( 'unternehmen_leitbild_3_text' );
$unternehmen_leitbild_3_icon         = get_field( 'unternehmen_leitbild_3_icon' );
$unternehmen_leitbild_4_ueberschrift = get_field( 'unternehmen_leitbild_4_ueberschrift' );
$unternehmen_leitbild_4_text         = get_field( 'unternehmen_leitbild_4_text' );
$unternehmen_leitbild_4_icon         = get_field( 'unternehmen_leitbild_4_icon' );


// GRUENDE
$gruende_ueberschrift = get_field( 'gruende_ueberschrift' );
$gruende_grund1_titel = get_field( 'gruende_grund1_titel' );
$gruende_grund1_text  = get_field( 'gruende_grund1_text' );
$gruende_grund2_titel = get_field( 'gruende_grund2_titel' );
$gruende_grund2_text  = get_field( 'gruende_grund2_text' );
$gruende_grund3_titel = get_field( 'gruende_grund3_titel' );
$gruende_grund3_text  = get_field( 'gruende_grund3_text' );
$gruende_grund4_titel = get_field( 'gruende_grund4_titel' );
$gruende_grund4_text  = get_field( 'gruende_grund4_text' );

// Kontakt
$kontakt_titel                       = get_field( 'kontakt_titel' );
$kontakt_checkbox_1                  = get_field( 'kontakt_checkbox_1' );
$kontakt_checkbox_2                  = get_field( 'kontakt_checkbox_2' );
$kontakt_globaler_hinweistext_fehler = get_field( 'kontakt_globaler_hinweistext_fehler' );
$kontakt_globaler_hinweistext_erfolg = get_field( 'kontakt_globaler_hinweistext_erfolg' );
$kontakt_name_platzhalter            = get_field( 'kontakt_name_platzhalter' );
$kontakt_name_hinweistext            = get_field( 'kontakt_name_hinweistext' );
$kontakt_vorname_platzhalter         = get_field( 'kontakt_vorname_platzhalter' );
$kontakt_vorname_hinweistext         = get_field( 'kontakt_vorname_hinweistext' );
$kontakt_email_hinweistext           = get_field( 'kontakt_email_hinweistext' );
$kontakt_email_platzhalter           = get_field( 'kontakt_email_platzhalter' );
$kontakt_email_hinweistext_format    = get_field( 'kontakt_email_hinweistext_format' );
$kontakt_telefon_hinweistext         = get_field( 'kontakt_telefon_hinweistext' );
$kontakt_telefon_platzhalter         = get_field( 'kontakt_telefon_platzhalter' );
$kontakt_wunschtermin_platzhalter    = get_field( 'kontakt_wunschtermin_platzhalter' );
$kontakt_wunschtermin_hinweistext    = get_field( 'kontakt_wunschtermin_hinweistext' );
$kontakt_interessen_platzhalter      = get_field( 'kontakt_interessen_platzhalter' );
$kontakt_interessen_ARRAY            = explode( ",", trim( get_field( 'kontakt_interessen' ) ) );
$kontakt_infobox_links               = get_field( 'kontakt_infobox_links' );
$kontakt_infobox_mitte               = get_field( 'kontakt_infobox_mitte' );
$kontakt_infobox_rechts              = get_field( 'kontakt_infobox_rechts' );
$kontakt_captcha_hinweistext         = get_field( 'kontakt_captcha_hinweistext' );
$kontakt_captcha_hinweistext_fehler  = get_field( 'kontakt_captcha_hinweistext_fehler' );
$kontakt_senden_button_text          = get_field( 'kontakt_senden_button_text' );
$kontakt_email_to                    = get_field( 'kontakt_email_to' );
$kontakt_bestaetigung_betreff        = get_field( 'kontakt_bestaetigung_betreff' );
$kontakt_bestaetigung_text           = get_field( 'kontakt_bestaetigung_text' );
$kontakt_bestaetigung_absender       = get_field( 'kontakt_bestaetigung_absender' );

get_header(); ?>
<!-- TESTIMONIALS -->
<?php
$args         = array(
	'posts_per_page'   => '20',
	'offset'           => 0,
	'category'         => '',
	'category_name'    => '',
	'orderby'          => 'date',
	'order'            => 'DESC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'testimonial',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'author'           => '',
	'author_name'      => '',
	'post_status'      => 'publish',
	'suppress_filters' => false
);
$testimonials = get_posts( $args );
?>
<section id="testimonials" class="testimonials-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div id="testimonials-carousel" class="carousel slide" data-ride="carousel" data-interval="5000">
                    <!-- Indicators -->
                    <ol class="testimonials-carousel-indicators carousel-indicators">
						<?php
						foreach ( $testimonials as $key => $post ) {
							if ( $key == 0 ) {
								echo '<li data-target="#testimonials-carousel" data-slide-to="' . $key . '" class="active"></li>';
							} else {
								echo '<li data-target="#testimonials-carousel" data-slide-to="' . $key . '" ></li>';
							}
						}
						?>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
						<?php
						foreach ( $testimonials as $key => $post ) {
							$bild   = wp_get_attachment_image( get_post_meta( get_the_ID(), 'testimonial_bild', true ), 'full' );
							$name   = get_post_meta( get_the_ID(), 'testimonial_name', true );
							$zitat  = apply_filters( 'meta_content', get_post_meta( get_the_ID(), 'testimonial_zitat', true ) );
							$active = '';
							if ( $key == 0 ) {
								$active = 'active';
							} else {
								$active = '';
							}
							echo '<div class="item ' . $active . '">
                                        <div class="row testimonial">
                                            <div class="col-sm-12 col-md-4 testimonial-image-col">
                                            ' . $bild . '
                                            </div>
                                            <div class="col-sm-12 col-md-8">
                                                <blockquote>' . $zitat . '
                                                    <cite>&mdash; ' . $name . '</cite>
                                                </blockquote>
                                            </div>
                                        </div>
                                  </div>';
						}
						?>
                    </div>
                </div>
            </div>
        </div>
</section>

<!-- SPOT -->

<section id="spot" class="spot-section"
    <?php if ( ! empty( $spot_bild ) ) : ?>style="background: url('<?php echo $spot_bild['url'] ?>') no-repeat"<?php endif; ?>>
    <div class="container">
        <div class="row spot-row">
            <div class="col-sm-12">
                <a class="popup-youtube spot-link" href="<?php echo $spot_youtube_link ?>" target="_blank">
                    <img src="<?php bloginfo( 'stylesheet_directory' ); ?>/assets/img/play.png" alt="">
                </a>
                <h3>VIDEO ANSEHEN</h3>
            </div>
        </div>
    </div>
</section>

<!-- STELLEN -->
<section id="stellen" class="stellen-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 stellen-text-container">
                <h3 class="stellen-title"><?php echo $stellen_ueberschrift ?></h3>
                <p><?php echo $stellen_untertitel ?>
                </p>
            </div>
            <div class="row stellen-row">
                <div class="col-sm-4 stellen-kategorie-container">
                    <h4 class="stellen-kategorie-title"><?php echo $stellen_kategorie_1 ?></h4>
					<?php echo $stellen_in_kategorie_1 ?>
                </div>
                <div class="col-sm-4 stellen-kategorie-container">
                    <h4 class="stellen-kategorie-title"><?php echo $stellen_kategorie_2 ?></h4>
					<?php echo $stellen_in_kategorie_2 ?>
                </div>
                <div class="col-sm-4 stellen-kategorie-container">
                    <h4 class="stellen-kategorie-title"><?php echo $stellen_kategorie_3 ?></h4>
					<?php echo $stellen_in_kategorie_3 ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- EINRICHTUNGEN -->
<?php
$args          = array(
	'posts_per_page'   => '20',
	'offset'           => 0,
	'category'         => '',
	'category_name'    => '',
	'orderby'          => 'date',
	'order'            => 'DESC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'einrichtung',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'author'           => '',
	'author_name'      => '',
	'post_status'      => 'publish',
	'suppress_filters' => false
);
$einrichtungen = get_posts( $args );
?>
<section id="einrichtungen" class="einrichtungen-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 einrichtungen-text-container">
                <h3 class="einrichtungen-title"><?php echo $einrichtungen_titel ?></h3>
                <p><?php echo $einrichtungen_text ?></p>
            </div>
        </div>
        <div class="row">
            <div id="einrichtungen-carousel" class="carousel slide" data-ride="carousel" data-interval="8000">
                <!-- Indicators -->
                <ol class="carousel-indicators">
					<?php
					foreach ( $einrichtungen as $key => $post ) {
						if ( $key == 0 ) {
							echo '<li data-target="#einrichtungen-carousel" data-slide-to="' . $key . '" class="active"></li>';
						} else {
							echo '<li data-target="#einrichtungen-carousel" data-slide-to="' . $key . '" ></li>';
						}
					}
					?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
					<?php
					foreach ( $einrichtungen as $key => $post ) {
						$title        = get_the_title();
						$beschreibung = get_post_meta( get_the_ID(), 'einrichtung_beschreibung', true );
						$bild         = wp_get_attachment_url( get_post_meta( get_the_ID(), 'einrichtung_bild', true ) );
						$active       = '';
						if ( $key == 0 ) {
							$active = 'active';
						} else {
							$active = '';
						}
						echo '<div class="item ' . $active . ' einrichtungen-slide col-sm-12" style="background: url(' . $bild . ') no-repeat;">
                                        <div class="carousel-caption">
                                            <h4 class="einrichtungen-slide-title">' . $title . '</h4>
                                            <p class="einrichtungen-slide-beschreibung">' . $beschreibung . '</p>
                                        </div>
                             </div>';
					}
					?>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#einrichtungen-carousel" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#einrichtungen-carousel" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</section>

<!-- GOOGLE MAP -->
<!--
<section id="googlemap" class="googlemap-section">
    <div id="map" style="height: 600px; width: 100%;"/>
    <script>
        function initMap() {
            var locations = [];
            var contentString = "";

			<?php
			foreach ( $einrichtungen as $key => $post ) {
				$title     = get_the_title();
				$adresse   = get_post_meta( get_the_ID(), 'einrichtung_adresse', true );
				$latitude  = get_post_meta( get_the_ID(), 'einrichtung_latitude', true );
				$longitude = get_post_meta( get_the_ID(), 'einrichtung_longitude', true );
				echo 'contentString = ' . json_encode( $adresse ) . ';';
				echo 'locations.push(["' . $title . '", contentString, ' . $latitude . ', ' . $longitude . ', ' . $key . ']);';
			}
			?>

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                center: new google.maps.LatLng(48.031239, 12.240515),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][2], locations[i][3]),
                    map: map
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(locations[i][1]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }
    </script>
</section>
-->
<!-- STELLENANZEIGEN -->
<section id="stellenanzeigen" class="stellenanzeigen-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 stellenanzeigen-text-container">
                <h3 class="stellenanzeigen-title"><?php echo $stellenanzeigen_title ?></h3>
            </div>
        </div>
		<?php
		switch_to_blog( 1 );

		$locations = array();
		$terms     = get_terms( [
			'taxonomy'   => 'job_listing_region',
			'hide_empty' => true
		] );
		foreach ( $terms as $term ) {
			$locations[] = $term->name;
		}

		foreach ( $locations as $location ) {
			$posts_array = get_posts( array(
					'posts_per_page' => 100,
					'post_type'      => 'job_listing',
					'post_status'    => 'publish',
					'orderby'        => 'date',
					'order'          => 'DESC',
					'tax_query'      => array(
						array(
							'taxonomy' => 'job_listing_region',
							'field'    => 'name',
							'terms'    => $location
						)
					)
				)
			);
			echo '<div class="row stellenanzeigen-row">
                    <div class="col-sm-12">
                        <h4 class="stellenanzeigen-location">' . $location . '</h4>';
			foreach ( $posts_array as $post ) {
				$post_link = "'" . get_permalink() . "'";
				echo '<div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-10 stellenanzeigen-stelle-title">
                        <span>' . $post->post_title . '</span>
                            </div>
                            <div class="col-sm-12 col-md-2" style="text-align:right;">
                                <button type="button" class="btn btn-warning" onclick="window.open(' . $post_link . ')">' . $stellenanzeigen_bewerben_button . '</button>
                            </div>
                        </div>
                    </div>
                </div>';
			}
		}
		restore_current_blog();
		?>
    </div>
    </div>
    </div>
</section>

<!-- NEUIGKEITEN -->
<section id="neuigkeiten" class="neuigkeiten-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 neuigkeiten-text-container">
                <h3 class="neuigkeiten-title"><?php echo $neuigkeiten_title ?></h3>
                <p><?php echo $neuigkeiten_text ?></p>
            </div>
        </div>
        <div class="row">
			<?php
			switch_to_blog( 1 );
			$args        = array(
				'posts_per_page'   => 3,
				'offset'           => 0,
				'category'         => '',
				'category_name'    => '',
				'orderby'          => 'date',
				'order'            => 'DESC',
				'include'          => '',
				'exclude'          => '',
				'meta_key'         => '',
				'meta_value'       => '',
				'post_type'        => 'post',
				'tag'              => 'Mitarbeiternews',
				'post_mime_type'   => '',
				'post_parent'      => '',
				'author'           => '',
				'author_name'      => '',
				'post_status'      => 'publish',
				'suppress_filters' => false
			);
			$posts_array = get_posts( $args );
			foreach ( $posts_array as $post ) {
				$title     = get_the_title();
				$date      = get_the_date();
				$thumbnail = get_the_post_thumbnail();
				$post_link = "'" . get_permalink() . "'";

				$content = wp_strip_all_tags( get_extended( get_post_field( 'post_content' ) )['main'] );
				$content = strlen( $content ) > 150 ? substr( $content, 0, 150 ) . "..." : $content;

				echo '<div class="col-sm-6 col-md-4">
                <div class="thumbnail neuigkeiten-neuigkeit">
                    ' . $thumbnail . '
                    <div class="caption">
                        <p class="neuigkeiten-date">' . $date . '</p>
                        <h4>' . $title . '</h4>
                        <p>' . $content . '</p>
                        <p><button type="button" class="btn btn-info neuigkeiten-button" onclick="window.open(' . $post_link . ')">' . $neuigkeiten_button . '</button></p>
                    </div>
                </div>
            </div>';
			}

			restore_current_blog();
			?>
        </div>
    </div>
</section>
<!-- FAQS -->
<section id="faqs" class="faqs-section">
    <div class="container">
        <div class="section-header faqs-text-container">
            <h3 class="faqs-title"><?php echo $faqs_ueberschrift ?></h3>
        </div>
        <div class="panel-group faqs-panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default faqs-panel">
                <div class="panel-heading faqs-faq-heading" role="tab" id="heading1">
                    <h4 class="panel-title faqs-faq-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1"
                           aria-expanded="true" aria-controls="collapse1">
							<?php echo $faqs_faq1_frage ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body faqs-faq-body">
						<?php echo $faqs_faq1_antwort ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default faqs-panel">
                <div class="panel-heading faqs-faq-heading" role="tab" id="heading2">
                    <h4 class="panel-title faqs-faq-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2"
                           aria-expanded="true" aria-controls="collapse2" class="collapsed">
							<?php echo $faqs_faq2_frage ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                    <div class="panel-body faqs-faq-body">
						<?php echo $faqs_faq2_antwort ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default faqs-panel">
                <div class="panel-heading faqs-faq-heading" role="tab" id="heading3">
                    <h4 class="panel-title faqs-faq-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3"
                           aria-expanded="true" aria-controls="collapse3" class="collapsed">
							<?php echo $faqs_faq3_frage ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                    <div class="panel-body faqs-faq-body">
						<?php echo $faqs_faq3_antwort ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default faqs-panel">
                <div class="panel-heading faqs-faq-heading" role="tab" id="heading4">
                    <h4 class="panel-title faqs-faq-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4"
                           aria-expanded="true" aria-controls="collapse4" class="collapsed">
							<?php echo $faqs_faq4_frage ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                    <div class="panel-body faqs-faq-body">
						<?php echo $faqs_faq4_antwort ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default faqs-panel">
                <div class="panel-heading faqs-faq-heading" role="tab" id="heading5">
                    <h4 class="panel-title faqs-faq-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5"
                           aria-expanded="true" aria-controls="collapse5" class="collapsed">
							<?php echo $faqs_faq5_frage ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                    <div class="panel-body faqs-faq-body">
						<?php echo $faqs_faq5_antwort ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default faqs-panel">
                <div class="panel-heading faqs-faq-heading" role="tab" id="heading6">
                    <h4 class="panel-title faqs-faq-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6"
                           aria-expanded="true" aria-controls="collapse6" class="collapsed">
							<?php echo $faqs_faq6_frage ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                    <div class="panel-body faqs-faq-body">
						<?php echo $faqs_faq6_antwort ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default faqs-panel">
                <div class="panel-heading faqs-faq-heading" role="tab" id="heading7">
                    <h4 class="panel-title faqs-faq-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7"
                           aria-expanded="true" aria-controls="collapse7" class="collapsed">
							<?php echo $faqs_faq7_frage ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                    <div class="panel-body faqs-faq-body">
						<?php echo $faqs_faq7_antwort ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default faqs-panel">
                <div class="panel-heading faqs-faq-heading" role="tab" id="heading8">
                    <h4 class="panel-title faqs-faq-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8"
                           aria-expanded="true" aria-controls="collapse8" class="collapsed">
							<?php echo $faqs_faq8_frage ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
                    <div class="panel-body faqs-faq-body">
						<?php echo $faqs_faq8_antwort ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default faqs-panel">
                <div class="panel-heading faqs-faq-heading" role="tab" id="heading9">
                    <h4 class="panel-title faqs-faq-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9"
                           aria-expanded="true" aria-controls="collapse9" class="collapsed">
							<?php echo $faqs_faq9_frage ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
                    <div class="panel-body faqs-faq-body">
						<?php echo $faqs_faq9_antwort ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default faqs-panel">
                <div class="panel-heading faqs-faq-heading" role="tab" id="heading10">
                    <h4 class="panel-title faqs-faq-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse10"
                           aria-expanded="true" aria-controls="collapse10" class="collapsed">
							<?php echo $faqs_faq10_frage ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
                    <div class="panel-body faqs-faq-body">
						<?php echo $faqs_faq10_antwort ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- FAKTEN -->
<section id="fakten" class="fakten-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 fakten-text-container">
                <h3 class="fakten-title"><?php echo $fakten_ueberschrift ?></h3>
                <p><?php echo $fakten_untertitel ?></p>
            </div>
        </div>
        <div class="row fakten-row">
            <div class="col-md-3 fakten-fakt">
                <i class="fa <?php echo $fakten_fakt1_icon ?>"></i>
                <h3 class="fakten-fakt-title counter"><?php echo $fakten_fakt1_zahl ?></h3>
                <p><?php echo $fakten_fakt1_text ?></p>
            </div>
            <div class="col-md-3 fakten-fakt">
                <i class="fa <?php echo $fakten_fakt2_icon ?>"></i>
                <h3 class="fakten-fakt-title counter"><?php echo $fakten_fakt2_zahl ?></h3>
                <p><?php echo $fakten_fakt2_text ?></p>
            </div>
            <div class="col-md-3 fakten-fakt">
                <i class="fa <?php echo $fakten_fakt3_icon ?>"></i>
                <h3 class="fakten-fakt-title counter"><?php echo $fakten_fakt3_zahl ?></h3>
                <p><?php echo $fakten_fakt3_text ?></p>
            </div>
            <div class="col-md-3 fakten-fakt">
                <i class="fa <?php echo $fakten_fakt4_icon ?>"></i>
                <h3 class="fakten-fakt-title counter"><?php echo $fakten_fakt4_zahl ?></h3>
                <p><?php echo $fakten_fakt4_text ?></p>
            </div>
        </div>
    </div>
</section>

<!-- UNTERNEHMEN -->
<section id="unternehmen" class="unternehmen-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 unternehmen-text-container">
                <h3 class="unternehmen-title"><?php echo $unternehmen_titel ?></h3>
                <p><?php echo $unternehmen_text ?></p>
            </div>
            <div class="col-sm-12 col-md-12 unternehmen-leitbild-container">
                <h3 class="unternehmen-leitbild-title"><?php echo $unternehmen_leitbild_ueberschrift ?></h3>
                <div id="unternehmen-leitbild-carousel" class="carousel vertical slide" data-interval="false"
                     data-wrap="false">
                    <div class="carousel-inner unternehmen-leitbild-carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="col-sm-12 col-md-4">
                                <i class="leitbild-image fa <?php echo $unternehmen_leitbild_1_icon ?>"
                                   aria-hidden="true"></i>
                            </div>
                            <div class="col-sm-12 col-md-8">
                                <h4 class="ticker-headline leitbild-headline"><?php echo $unternehmen_leitbild_1_ueberschrift ?></h4>
                                <span class="leitbild-text"><?php echo $unternehmen_leitbild_1_text ?></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-sm-12 col-md-4">
                                <i class="leitbild-image fa <?php echo $unternehmen_leitbild_2_icon ?>"
                                   aria-hidden="true"></i>
                            </div>
                            <div class="col-sm-12 col-md-8">
                                <h4 class="ticker-headline leitbild-headline"><?php echo $unternehmen_leitbild_2_ueberschrift ?></h4>
                                <span class="leitbild-text"><?php echo $unternehmen_leitbild_2_text ?></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-sm-12 col-md-4">
                                <i class="leitbild-image fa <?php echo $unternehmen_leitbild_3_icon ?>"
                                   aria-hidden="true"></i>
                            </div>
                            <div class="col-sm-12 col-md-8">
                                <h4 class="ticker-headline leitbild-headline"><?php echo $unternehmen_leitbild_3_ueberschrift ?></h4>
                                <span class="leitbild-text"><?php echo $unternehmen_leitbild_3_text ?></span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-sm-12 col-md-4">
                                <i class="leitbild-image fa <?php echo $unternehmen_leitbild_4_icon ?>"
                                   aria-hidden="true"></i>
                            </div>
                            <div class="col-sm-12 col-md-8">
                                <h4 class="ticker-headline leitbild-headline"><?php echo $unternehmen_leitbild_4_ueberschrift ?></h4>
                                <span class="leitbild-text"><?php echo $unternehmen_leitbild_4_text ?></span>
                            </div>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="up carousel-control" href="#unternehmen-leitbild-carousel" role="button"
                       data-slide="prev">
                        <span class="fa fa-chevron-up" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="down carousel-control" href="#unternehmen-leitbild-carousel" role="button"
                       data-slide="next">
                        <span class="fa fa-chevron-down" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- GRUENDE -->
<section id="gruende" class="gruende-section">
    <div class="container">
        <div class="section-header gruende-text-container">
            <h3 class="gruende-title"><?php echo $gruende_ueberschrift ?></h3>
        </div>
        <div>

            <!-- Nav tabs -->
            <ul class="nav nav-tabs row gruende-ul" role="tablist">
                <li role="presentation" class="active col-md-3 col-sm-12 gruende-grund-title">
                    <a href="#grund1" aria-controls="grund1" role="tab"
                       data-toggle="tab"><?php echo $gruende_grund1_titel ?></a>
                </li>
                <li role="presentation" class="col-md-3 col-sm-12 gruende-grund-title">
                    <a href="#grund2" aria-controls="grund2" role="tab"
                       data-toggle="tab"><?php echo $gruende_grund2_titel ?></a>
                </li>
                <li role="presentation" class="col-md-3 col-sm-12 gruende-grund-title">
                    <a href="#grund3" aria-controls="grund3" role="tab"
                       data-toggle="tab"><?php echo $gruende_grund3_titel ?></a>
                </li>
                <li role="presentation" class="col-md-3 col-sm-12 gruende-grund-title">
                    <a href="#grund4" aria-controls="grund4" role="tab"
                       data-toggle="tab"><?php echo $gruende_grund4_titel ?></a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="grund1">
					<?php echo $gruende_grund1_text ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="grund2">
					<?php echo $gruende_grund2_text ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="grund3">
					<?php echo $gruende_grund3_text ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="grund4">
					<?php echo $gruende_grund4_text ?>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- KONTAKTFORMULAR -->
<?php
if ( isset( $_POST['submitted'] ) ) {

	if ( trim( $_POST['checkbox'] ) != '' ) {
		$mail_checkbox = trim( $_POST['checkbox'] );
	}

	if ( trim( $_POST['nachname'] ) === '' ) {
		$nachnameError = $kontakt_name_hinweistext;
		$hasError      = true;
	} else {
		$mail_nachname = trim( $_POST['nachname'] );
	}

	if ( trim( $_POST['vorname'] ) === '' ) {
		$vornameError = $kontakt_vorname_hinweistext;
		$hasError     = true;
	} else {
		$mail_vorname = trim( $_POST['vorname'] );
	}

	if ( trim( $_POST['email'] ) === '' ) {
		$emailError = $kontakt_email_hinweistext;
		$hasError   = true;
	} else if ( ! preg_match( "/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim( $_POST['email'] ) ) ) {
		$emailError = $kontakt_email_hinweistext_format;
		$hasError   = true;
	} else {
		$mail_email = trim( $_POST['email'] );
	}

	if ( trim( $_POST['telefon'] ) === '' ) {
		$telefonError = $kontakt_telefon_hinweistext;
		$hasError     = true;
	} else {
		$mail_telefon = trim( $_POST['telefon'] );
	}

	if ( trim( $_POST['termin'] ) === '' ) {
		$terminError = $kontakt_wunschtermin_hinweistext;
		$hasError    = true;
	} else {
		$mail_termin = trim( $_POST['termin'] );
	}

	if ( trim( $_POST['interesse'] ) != '' ) {
		$mail_interesse = trim( $_POST['interesse'] );
	}
/*
	if ( trim( $_POST['g-recaptcha-response'] ) === '' ) {
		$captchaError = $kontakt_captcha_hinweistext;
		$hasError     = true;
	} else {
		$url  = 'https://www.google.com/recaptcha/api/siteverify';
		$data = array(
			'secret'   => '6LfEdToUAAAAANqS0oirlM2K1IU4AiEcGSM6gFRw',
			'response' => $_POST['g-recaptcha-response']
		);

		$options       = array(
			'http' => array(
				'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'  => 'POST',
				'content' => http_build_query( $data )
			)
		);
		$context       = stream_context_create( $options );
		$result        = file_get_contents( $url, false, $context );
		$result_object = json_decode( $result );
		if ( $result_object->{'success'} != 1 ) {
			$captchaError = $kontakt_captcha_hinweistext_fehler;
			$hasError     = true;
		}

	}
	*/

	if ( ! isset( $hasError ) ) {

		//send mail to Krohn-Leitmannstetter
		$emailTo = $kontakt_email_to;
		$subject = '[jobs.krohn-leitmannstetter.de] Anfrage von ' . $mail_vorname . ' ' . $mail_nachname;
		$body    = "$mail_checkbox \n\nName: $mail_vorname $mail_nachname \n\nE-Mail: $mail_email \n\n Telefon: $mail_telefon \n\n Wunschtermin: $mail_termin \n\nInteresse: $mail_interesse";
		$headers = 'From: ' . $mail_vorname . ' ' . $mail_nachname . ' <' . $mail_email . '>' . "\r\n" . 'Reply-To: ' . $mail_email;
		wp_mail( $emailTo, $subject, $body, $headers );

		//send confirmation mail to visitor
		$emailTo = $mail_email;
		$subject = $kontakt_bestaetigung_betreff;
		$body    = $kontakt_bestaetigung_text;
		$headers = $kontakt_bestaetigung_absender;
		wp_mail( $emailTo, $subject, $body, $headers );

		$emailSent = true;
	}

} ?>
<section id="kontakt" class="kontakt-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="kontakt-title">Jetzt Kontakt aufnehmen!</h3>
            </div>
        </div>
        <div class="row">
			<?php if ( isset( $emailSent ) && $emailSent == true ) { ?>
                <div class="thanks col-lg-12 col-md-12">
                    <p><?php echo $kontakt_globaler_hinweistext_erfolg ?></p>
                </div>
			<?php } else { ?>
                <form action="#kontakt" method="post" id="contactform" class="contact-form">
                    <div class="col-lg-8 col-lg-offset-3 contact-form-checkboxes">
                        <div class="checkbox">
                            <input type="radio" id="checkbox1" name="checkbox"
                                   value="<?php echo $kontakt_checkbox_1 ?>" checked>
                            <label for="checkbox1"><?php echo $kontakt_checkbox_1 ?></label>
                        </div>
                        <div class="checkbox">
                            <input type="radio" id="checkbox2" name="checkbox"
                                   value="<?php echo $kontakt_checkbox_2 ?>"
                            >
                            <label for="checkbox2"><?php echo $kontakt_checkbox_2 ?></label>
                        </div>
                    </div>
					<?php if ( isset( $hasError ) ) { ?>
                        <div class="col-lg-12 col-md-12">
                            <p class="error global"><?php echo $kontakt_globaler_hinweistext_fehler ?></p>
                        </div>
					<?php } ?>
                    <div class="col-lg-6 col-md-6">
						<?php if ( $nachnameError != '' ) { ?>
                            <span class="error"><?= $nachnameError; ?></span>
						<?php } ?>
                        <input type="text" id="nachname" name="nachname"
                               placeholder="<?php echo $kontakt_name_platzhalter ?>"
                               class="<?php if ( $nachnameError != '' ) {
							       echo "missing";
						       } ?>"
                               value="<?php if ( isset( $_POST['nachname'] ) ) {
							       echo $_POST['nachname'];
						       } ?>">
						<?php if ( $vornameError != '' ) { ?>
                            <span class="error"><?= $vornameError; ?></span>
						<?php } ?>
                        <input type="text" id="vorname" name="vorname"
                               placeholder="<?php echo $kontakt_vorname_platzhalter ?>"
                               class="<?php if ( $vornameError != '' ) {
							       echo "missing";
						       } ?>"
                               value="<?php if ( isset( $_POST['vorname'] ) ) {
							       echo $_POST['vorname'];
						       } ?>">
						<?php if ( $emailError != '' ) { ?>
                            <span class="error"><?= $emailError; ?></span>
						<?php } ?>
                        <input type="email" id="email" name="email"
                               placeholder="<?php echo $kontakt_email_platzhalter ?>"
                               class="<?php if ( $emailError != '' ) {
							       echo "missing";
						       } ?>"
                               value="<?php if ( isset( $_POST['email'] ) ) {
							       echo $_POST['email'];
						       } ?>">
                    </div>
                    <div class="col-lg-6 col-md-6">
						<?php if ( $telefonError != '' ) { ?>
                            <span class="error"><?= $telefonError; ?></span>
						<?php } ?>
                        <input type="text" id="telefon" name="telefon"
                               placeholder="<?php echo $kontakt_telefon_platzhalter ?>"
                               class="<?php if ( $telefonError != '' ) {
							       echo "missing";
						       } ?>"
                               value="<?php if ( isset( $_POST['telefon'] ) ) {
							       echo $_POST['telefon'];
						       } ?>">
						<?php if ( $terminError != '' ) { ?>
                            <span class="error"><?= $terminError; ?></span>
						<?php } ?>
                        <input type="text" id="termin" name="termin"
                               placeholder="<?php echo $kontakt_wunschtermin_platzhalter ?>"
                               class="<?php if ( $terminError != '' ) {
							       echo "missing";
						       } ?>"
                               value="<?php if ( isset( $_POST['termin'] ) ) {
							       echo $_POST['termin'];
						       } ?>">
						<?php if ( $interesseError != '' ) { ?>
                            <span class="error"><?= $interesseError; ?></span>
						<?php } ?>
                        <select name="interesse" id="interesse" class="selectpicker kontakt-interesse bs-select-hidden">
                            <option value=""<?php if ( $_POST['interesse'] === "" ) {
								echo " selected";
							} ?>><?php echo $kontakt_interessen_platzhalter ?>
                            </option>
							<?php
							foreach ( $kontakt_interessen_ARRAY as $interesse ) {
								?>
                                <option value="<?= $interesse; ?>"<?php if ( $_POST['interesse'] === $interesse ) {
									echo " selected";
								} ?>><?= $interesse; ?>
                                </option>
								<?php
							}
							?>
                        </select>
                    </div>
                    <div class="col-lg-12 kontakt-infobox">
                        <div class="col-lg-3 col-md-12">
                            <p><?php echo $kontakt_infobox_links ?></p>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <p><?php echo $kontakt_infobox_mitte ?></p>
                        </div>
                        <div class="col-lg-3 col-md-12">
                            <p><?php echo $kontakt_infobox_rechts ?></p>
                        </div>
                    </div>
                    <!--
                    <div class="col-lg-12 col-md-12 kontakt-captcha-col">
						<?php if ( $captchaError != '' ) { ?>
                            <span class="error"><?= $captchaError; ?></span>
						<?php } ?>
                        <div class="g-recaptcha" data-sitekey="6LfEdToUAAAAAMdhTfClggPVxhvDaRN4HXlM1qVU"></div>
                    </div>
                    -->
                    <div class="col-lg-12 col-md-12 kontakt-button-col">
                        <button type="submit"
                                class="btn btn-info kontakt-button"><?php echo $kontakt_senden_button_text ?></button>
                    </div>
                    <input type="hidden" name="submitted" id="submitted" value="true"/>
                </form>
			<?php } ?>
        </div>
</section>

<!-- MODAL
================================================== -->
<!--
<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
                <iframe width="800" height="450" src="<?php echo $spot_youtube_link ?>" frameborder="0"
                        gesture="media" allow="encrypted-media" allowfullscreen></iframe>
            </div>

        </div>
    </div>
</div>
-->
<?php get_footer( 'custom' ); ?>
