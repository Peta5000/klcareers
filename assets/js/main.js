$(function () {

    // Cache the window object
    var $window = $(window);

    // Parallax background effect
    $('section[data-type="background"]').each(function () {

        var $bgobj = $(this); //assigning the object

        $(window).scroll(function () {
            // scroll the background at var speed
            // the yPos is a negative value because we're scrolling UP!

            var yPos = -($window.scrollTop() / $bgobj.data('speed'));

            // Put together our final background position
            var coords = '50% ' + yPos + 'px';

            // Move the background
            $bgobj.css({backgroundPosition: coords});
        }); // end window scroll
    });
});

$(window).scroll(function () {
    if ($(document).scrollTop() > 500) {
        $('.navbar').addClass('shrink');
        /*$('.menu-image-not-hovered').addClass('shrink');*/
        $('.navbar-nav').addClass('shrink');
        $('.nav.navbar-nav').addClass('shrink');
    } else {
        $('.navbar').removeClass('shrink');
        /*$('.menu-image-not-hovered').removeClass('shrink');*/
        $('.navbar-nav').removeClass('shrink');
        $('.nav.navbar-nav').removeClass('shrink');
    }
});

window.sr = ScrollReveal({mobile: false});
sr.reveal('.spot-section');
sr.reveal('.stellen-section');
sr.reveal('.einrichtungen-section');
sr.reveal('.googlemap-section');
sr.reveal('.stellenanzeigen-section');
sr.reveal('.neuigkeiten-section');
sr.reveal('.faqs-section');
sr.reveal('.fakten-section');
sr.reveal('.unternehmen-section');
sr.reveal('.gruende-section');
sr.reveal('.kontakt-section');

jQuery(document).ready(function ($) {
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });
});

checkitem = function () {
    var $this;
    $this = $("#unternehmen-leitbild-carousel");
    if ($("#unternehmen-leitbild-carousel .carousel-inner .item:first").hasClass("active")) {
        $this.children(".up").hide();
        $this.children(".down").show();
    } else if ($("#unternehmen-leitbild-carousel .carousel-inner .item:last").hasClass("active")) {
        $this.children(".down").hide();
        $this.children(".up").show();
    } else {
        $this.children(".carousel-control").show();
    }
};
jQuery(document).ready(function ($) {
    checkitem();
});

$("#unternehmen-leitbild-carousel").on("slid.bs.carousel", "", checkitem);